[![Netlify Status](https://api.netlify.com/api/v1/badges/1085838f-84c4-4b10-ba93-bdbb2e7b3955/deploy-status)](https://app.netlify.com/sites/zeal-website/deploys)

## How to get going

In addition to git, install git lfs:
https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/
Linux: Packages for Debian and RPM are available from PackageCloud.
macOS: You can use Homebrew via "brew install git-lfs" or MacPorts via "port install git-lfs".
Windows: You can use the Chocolatey package manager via "choco install git-lfs".

This site is built using [hugo](https://gohugo.io).  Installation instructions can be found [here](https://gohugo.io/getting-started/installing/).

Once you've got hugo running, you can start the site with the command

```
hugo server -D
```

## Don't need SCSS with this anymore

### Use Semantic HTML

Use semantic HTML, then set class names on elements under `<main />`.

### CSS Class Structure

- main
 - slogan
 - stats
 - clients
 - terimonials
 - about
 - contact
 - products
 - advisors
 - experts

Target these in CSS to avoid global styling.

### TODO
- [ ]

generated pwa images with: pwabuilder https://www.zealitconsultants.com -i "/Users/kijanawoodard/Downloads/Zeal Logo - Full-01-1.png"

processed sailing background video with https://converterpoint.com/.

resized images following advice from https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/

To resize a new image:
`brew install imagemagick`
then run 
`./scripts/smush.sh INPUT_PATH RESIZE_WIDTH OUTPUT_PATH FORMAT_VALUE QUALITY_VALUE`
Quality value is set to 82 (recommended in article) if not set.

ex:
`./scripts/smush.sh 'static/img/team/*.*' 150 static/img/team/150x150/ jpg`
`./scripts/smush.sh 'static/img/team/*.*' 50 'static/img/team/50x50/' jpg`

`./scripts/smush.sh 'static/img/team/leong.jpg' 150 'static/img/team/150x150/' webp`