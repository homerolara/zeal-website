#!/bin/bash
input=$1
input_size=`identify -format "%w %h" $input`

output_path=${2:-$(dirname $input)}
filename=${input##*/}
basename=${filename%.*}
output=$output_path/$basename.webp

preset=${3:-default}
quality=${4:-75}
size=${5:-$input_size}

echo input file $input, output file $output, preset $preset, quality $quality, size $size

mkdir -p $output_path #make sure the folder exists
cwebp $input -preset $preset -q $quality -resize $size -o $output