"use strict";
//converted from class via https://babeljs.io/en/repl
function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if ((typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ImageCarousel =
/*#__PURE__*/
function () {
  function ImageCarousel() {
    var _this = this;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
      startPosition: 0
    };

    _classCallCheck(this, ImageCarousel);

    var defaultOptions = {
      startPosition: 0,
      autoplay: false,
      autoplayDelay: 3000
    };
    this.options = Object.assign(defaultOptions, options); // Store elements from website

    this.bullets = Array.prototype.slice.call(document.querySelectorAll(".bullet"), 0);
    this.sliderBullets = document.querySelector(".slider-bullets");
    this.slides = Array.prototype.slice.call(document.querySelectorAll(".slide"), 0);
    this.sliderGallery = document.querySelector(".slider-gallery");
    this.sliderNavigation = Array.prototype.slice.call(document.querySelectorAll(".slider-navigation"), 0);
    this.sliderNavigationLeft = document.querySelector(".slider-navigation_left");
    this.sliderNavigationRight = document.querySelector(".slider-navigation_right"); // calculate useful dimensions

    this.sliderBulletsLength = this.bullets[0].clientWidth * this.bullets.length; // bc display: none changes lenghth, a constant is defined first

    this.sliderGalleryLength = 0;
    this.slideLength = this.slides[0].scrollWidth;
    this.slidesOffset = 0;
    this.slidesPosition = 0;
    this.currentPosition = 0;
    this.visibleLength = 0; // visible length is amount of bullets that should be visible

    this.navigationDisabled = false;
    this.leftClones = [];
    this.rightClones = []; // Controls which element gets hidden and shown when navigating left or right

    this.leftIndex = -1, this.rightIndex = 0; // The current bullet and slide

    this.selected; // Iterate through all bullets and add event listeners

    var _loop = function _loop(i) {
      _this.currentPosition = i;

      _this.bullets[i].addEventListener('mouseover', function () {
        return _this.slideSwap(i);
      });

      _this.bullets[i].addEventListener('click', function () {
        return _this.slideSwap(i);
      });
    };

    for (var i = 0; i < this.bullets.length; i++) {
      _loop(i);
    } // Right navigation click


    this.sliderNavigationRight.addEventListener('click', function (event) {
      if (_this.rightIndex < _this.bullets.length - 1) {
        _this.bullets[++_this.leftIndex].classList.add('bullet-hide');

        _this.bullets[++_this.rightIndex].classList.remove('bullet-hide');

        _this.isNavigationEnd();

        _this.currentPosition = _this.rightIndex; // When on mobile mode, tapping right means switching the slide as well

        if (window.innerWidth < 996) {
          _this.slideSwap(_this.rightIndex);
        }
      }
    }); // Left navigation click

    this.sliderNavigationLeft.addEventListener('click', function (event) {
      if (_this.leftIndex > -1) {
        _this.leftIndex--;
        _this.rightIndex--;
        if(_this.bullets[_this.leftIndex]) _this.bullets[_this.leftIndex].classList.remove('bullet-hide');

        if(_this.bullets[_this.rightIndex]) _this.bullets[_this.rightIndex].classList.add('bullet-hide');

        _this.isNavigationEnd();

        _this.currentPosition = _this.leftIndex + 1; // When on mobile mode, tapping left means switching the slide as well

        if (window.innerWidth < 996) {
          _this.slideSwap(_this.leftIndex + 1);
        }
      }
    }); // Initialize 

    window.addEventListener("load", function () {
      _this.ImageCarouselInitialize();

      _this.setActive();

      if (_this.options.autoplay) {
        setInterval(function () {
          if (_this.currentPosition >= _this.slides.length) {
            _this.currentPosition = 0;

            if (!_this.navigationDisabled) {
              for (var _i = 0; _i < _this.slides.length; _i++) {
                _this.sliderNavigationLeft.dispatchEvent(new MouseEvent('click'));
              }
            }
          }

          if (_this.currentPosition >= _this.visibleLength) {
            if (!_this.navigationDisabled) {
              _this.sliderNavigationRight.dispatchEvent(new MouseEvent('click'));
            }
          }

          _this.slideSwap(_this.currentPosition);

          _this.currentPosition++;
        }, _this.options.autoplayDelay);
      }

      _this.setStartIndex(_this.options.startPosition);
    });
    window.addEventListener("resize", function () {
      var resizeTimer;
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(_this.ImageCarouselInitialize.bind(_this), 500);
    });
    window.addEventListener("orientationchange", this.ImageCarouselInitialize.bind(this));
  }

  _createClass(ImageCarousel, [{
    key: "setActive",
    value: function setActive() {
      // Make the current slide and bullet activated
      var bullet = this.bullets[this.currentPosition];
      if (bullet) bullet.classList.add('bullet-active');
      var slide = this.slides[this.currentPosition];
      if (slide) slide.classList.add('slide-active'); //  Select the default element used to swap 'active' classes

      this.selected = [document.querySelector('.bullet-active'), document.querySelector('.slide-active')];
    } // Check if we're at the very first element or last element of navigation
    // If we are, hide the respective buttons

  }, {
    key: "isNavigationEnd",
    value: function isNavigationEnd() {
      if (this.leftIndex <= -1) {
        // Here the opacity is set too so the CSS transition applies
        // this.sliderNavigationLeft.style.opacity = "0";
        // this.sliderNavigationLeft.style.visibility = "hidden";
      } else {
        this.sliderNavigationLeft.style.opacity = "";
        this.sliderNavigationLeft.style.visibility = "visible";
      }

      if (this.rightIndex >= this.bullets.length - 1) {
        // this.sliderNavigationRight.style.opacity = "0";
        // this.sliderNavigationRight.style.visibility = "hidden";
      } else {
        this.sliderNavigationRight.style.opacity = "";
        this.sliderNavigationRight.style.visibility = "visible";
      }
    } // Controls the layout and what gets shown
    // It's called when the viewport changes

  }, {
    key: "ImageCarouselInitialize",
    value: function ImageCarouselInitialize() {
      var _this2 = this;

      // By default, nothing should be active
      this.bullets.forEach(function (e) {
        return e.classList.remove('bullet-active');
      });
      this.slides.forEach(function (e) {
        e.classList.remove('slide-active');
        _this2.sliderGalleryLength += e.scrollWidth; // get total length for sliding purposes
      });
      this.setActive();
      this.slideSwap(this.currentPosition);
      this.leftIndex = -1 + this.currentPosition; // Makes it compatible with navigating right
      // Determines elements that should be hidden first

      if (this.sliderBulletsLength > this.sliderBullets.clientWidth - this.sliderNavigationLeft.clientWidth * 2 || this.bullets.length > 5) {
        if (window.innerWidth < 996) {
          // Here, we only want to display 1 bullet
          this.rightIndex = this.currentPosition;
          this.visibleLength = 1;
          this.navigationDisabled = true; // Make the navigation visible when there's more than one element

          if (this.bullets.length > 1) {
            this.sliderNavigation.forEach(function (e) {
              return e.style.visibility = "visible";
            });
            this.sliderNavigation.forEach(function (e) {
              return e.style.opacity = "";
            });
            this.isNavigationEnd();
            this.navigationDisabled = false;
          } // Hide the bullets until there are 1 left


          for (var i = 0; i < this.bullets.length; i++) {
            if (i != this.currentPosition) {
              this.bullets[i].classList.add("bullet-hide");
            } else this.bullets[i].classList.remove("bullet-hide");
          }
        } else if (window.innerWidth < 1440) {
          // Here, we want to display only 3 bullets
          this.visibleLength = 3; // This sets the bounds for which buttons are visible
          // This also handles the syncing

          this.rightIndex = this.currentPosition + 2;

          if (this.slides.length - this.currentPosition <= 2) {
            this.rightIndex = this.slides.length - 1;
            this.leftIndex = this.slides.length - 4;
          } // Hide the navigation
          // Since 3 elements will be shown, if there's already 3, there's no point to show the arrows


          if (this.bullets.length === 3 && false) {
            this.sliderNavigation.forEach(function (e) {
              return e.style.opacity = "0";
            });
            this.sliderNavigation.forEach(function (e) {
              return e.style.visibility = "hidden";
            });
            this.navigationDisabled = true;
          } else {
            this.sliderNavigation.forEach(function (e) {
              return e.style.opacity = "";
            });
            this.sliderNavigation.forEach(function (e) {
              return e.style.visibility = "visible";
            });
            this.isNavigationEnd();
            this.navigationDisabled = false;
          } // Hide the bullets until there are 3 left


          for (var _i2 = 0; _i2 < this.bullets.length; _i2++) {
            if (_i2 > this.leftIndex && _i2 <= this.rightIndex) this.bullets[_i2].classList.remove("bullet-hide");else {
              this.bullets[_i2].classList.add("bullet-hide");
            }
          }
        } else {
          // This is for larger viewports
          this.visibleLength = 5;

          if (this.bullets.length === 5 && false) {
            this.sliderNavigation.forEach(function (e) {
              return e.style.opacity = "0";
            });
            this.sliderNavigation.forEach(function (e) {
              return e.style.visibility = "hidden";
            });
            this.navigationDisabled = true;
          } else {
            this.sliderNavigation.forEach(function (e) {
              return e.style.opacity = "";
            });
            this.sliderNavigation.forEach(function (e) {
              return e.style.visibility = "visible";
            });
            this.isNavigationEnd();
            this.navigationDisabled = false;
          } // This sets the bounds for which buttons are visible
          // This also handles the syncing


          this.rightIndex = this.currentPosition + 4;

          if (this.slides.length - this.currentPosition <= 4) {
            this.rightIndex = this.slides.length - 1;
            this.leftIndex = this.slides.length - 6;
          }

          this.isNavigationEnd(); // Hide the bullets until there are 5 left

          for (var _i3 = 0; _i3 < this.bullets.length; _i3++) {
            if (_i3 > this.leftIndex && _i3 <= this.rightIndex) this.bullets[_i3].classList.remove("bullet-hide");else {
              this.bullets[_i3].classList.add("bullet-hide");
            }
          }
        }
      } else {
        // Here, there's no responsive issues with the width of the slider so just show it all
        // Since there's no responsive issues, there's no need for navigation
        // this.sliderNavigation.forEach(function (e) {
        //   return e.style.opacity = "0";
        // });
        // this.sliderNavigation.forEach(function (e) {
        //   return e.style.visibility = "hidden";
        // });
        this.navigationDisabled = true; // Since there's no responsive issues, nothing should be hidden

        for (var _i4 = 0; _i4 < this.bullets.length; _i4++) {
          this.bullets[_i4].classList.remove("bullet-hide");
        }
      }
    } // Swap a bullet and slide (switching to the next one)

  }, {
    key: "slideSwap",
    value: function slideSwap(index) {
      var _this3 = this;

      this.slideLength = this.slides[0].scrollWidth; // Swap bullet and slide active states

      if(this.selected[0]) this.selected[0].classList.remove('bullet-active');
      if(this.selected[1]) this.selected[1].classList.remove('slide-active');
      if(this.bullets[index]) this.bullets[index].classList.add('bullet-active');
      if(this.slides[index]) this.slides[index].classList.add('slide-active'); // The translation is based on index position and slide width

      this.slidesPosition = -index * this.slideLength;
      Array.from(this.sliderGallery.children).forEach(function (e) {
        e.style.transform = "translateX(".concat(_this3.slidesPosition + _this3.slidesOffset, "px)");
      }); // set new selected bullet for future swaps

      this.selected = [this.bullets[index], this.slides[index]];
    } // Change start index for ordering purposes

  }, {
    key: "setStartIndex",
    value: function setStartIndex(deltaIndex) {
      if (deltaIndex > 0 && deltaIndex < this.slides.length) {
        this.leftIndex += deltaIndex;
        this.rightIndex += deltaIndex;
        this.isNavigationEnd();
        this.bullets[this.leftIndex].classList.add('bullet-hide');
        this.bullets[this.rightIndex].classList.remove('bullet-hide');
        this.currentPosition = index;
        this.slideSwap(deltaIndex);
      }
    } // Offset adds clones of slides to remove any blank spaces or if you want to start at a different slide
    // Example: you want to start in the middle slide, not the start slide, so you set the offset to 1

  }, {
    key: "setOffset",
    value: function setOffset(offset) {
      this.leftClones.forEach(function (e) {
        return e.remove();
      });
      this.rightClones.forEach(function (e) {
        return e.remove();
      });

      for (var i = 0; i < offset; i++) {
        this.leftClones.unshift(this.sliderGallery.insertBefore(this.slides[this.slides.length - (i + 1)].cloneNode(true), this.sliderGallery.children[0]));
        this.rightClones.unshift(this.sliderGallery.appendChild(this.slides[i].cloneNode(true)));
      }
    }
  }]);

  return ImageCarousel;
}();